package org.asusual.mobile.ios.bubblegame {
    import starling.events.Event;
    import starling.core.Starling;
    import starling.display.MovieClip;
    import starling.textures.TextureAtlas;
    import starling.textures.Texture;
    import starling.display.Sprite;

    /**
     * @author Dmitry.Bezverkhiy
     */
    public class Bubble extends Sprite {
        
        [Embed(source="/../assets/bubbles.png")]
        private var Bubbles : Class;
        
        [Embed(source="/../assets/bubblesAtlas.xml", mimeType="application/octet-stream")]
        private var BubblesAtlas : Class;
        private var dieSmall : MovieClip;
        private var movieClip : MovieClip;
        private var _id : int;
        
        public function Bubble() {
            init();    
        }

        private function init() : void {
            var texture : Texture = Texture.fromBitmap(new Bubbles());
            var atlas : XML = XML(new BubblesAtlas());
            var textureAtlas : TextureAtlas = new TextureAtlas(texture,atlas);
            movieClip = new MovieClip(textureAtlas.getTextures("bubble_"),1);
            movieClip.loop = false;            
            movieClip.play();
            Starling.juggler.add(movieClip);
            addChild(movieClip);  
            var dieSmallTextures : Vector.<Texture> = new Vector.<Texture>();
            var dieSmall01 : Texture = textureAtlas.getTexture("die_s");
            var dieSmall02 : Texture = textureAtlas.getTexture("die_m");
            dieSmallTextures.push(dieSmall01);
            dieSmallTextures.push(dieSmall02);
            dieSmall = new MovieClip(dieSmallTextures);
            dieSmall.loop = false;
            dieSmall.addEventListener(Event.COMPLETE, onDie);
        }
    
        private function onDie(event : Event) : void {
            dispatchEvent(new BubbleEvent(BubbleEvent.DIE));
        }

        public function kill() : void {
            removeChild(movieClip);
            movieClip.stop();
            Starling.juggler.remove(movieClip);
            movieClip.dispose();
            movieClip = null;
            dieSmall.removeEventListener(Event.COMPLETE, onDie);
            dieSmall.dispose();
            dieSmall = null;
        }

        public function get id() : int {
            return _id;
        }

        public function set id(id : int) : void {
            _id = id;
        }

        public function destroy() : void {
            removeChild(movieClip);
            Starling.juggler.remove(movieClip);
            addChild(dieSmall);
            Starling.juggler.add(dieSmall);
        }
    }
}
