package org.asusual.mobile.ios.bubblegame {
    import starling.events.Event;

    /**
     * @author Dmitry.Bezverkhiy
     */
    public class BubbleEvent extends Event {
        public static const DIE : String = "DIE";
        
        public function BubbleEvent(type : String, bubbles : Boolean = false, data : Object = null) {
            super(type, bubbles, data);
        }
    }
}
