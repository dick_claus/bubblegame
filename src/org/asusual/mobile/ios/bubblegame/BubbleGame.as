package org.asusual.mobile.ios.bubblegame {
    import starling.core.Starling;

    import flash.display.Sprite;
    import flash.events.Event;

    [SWF(backgroundColor="#FFFFFF", frameRate="60", width="320", height="480")]

    public class BubbleGame extends Sprite {
        private var starling : Starling;
                
        
        public function BubbleGame() {
            addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
        }

        private function onAddedToStage(event : Event) : void {
            removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
            addEventListener(Event.ACTIVATE, onActivateHandler);
            addEventListener(Event.DEACTIVATE, onDeactivateHandler);
            init();
        }

        private function init() : void {
            starling = new Starling(Game,stage);
            //starling.showStats = true;
            //starling.enableErrorChecking = true;
            starling.start();            
        }

        private function onDeactivateHandler(event : Event) : void {
            starling.stop();
        }

        private function onActivateHandler(event : Event) : void {
            starling.start();
        }
    }
}
