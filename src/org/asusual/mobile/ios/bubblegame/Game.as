package org.asusual.mobile.ios.bubblegame {
    import starling.text.TextField;

    import flash.geom.Point;

    import starling.events.TouchPhase;
    import starling.events.Touch;
    import starling.events.TouchEvent;

    import flash.utils.clearInterval;
    import flash.utils.Dictionary;

    import starling.core.Starling;
    import starling.animation.Transitions;
    import starling.animation.Tween;
    import starling.display.Sprite;

    import flash.utils.setInterval;

    /**
     * @author Dmitry.Bezverkhiy
     */
    public class Game extends Sprite {
        private var bg : Background;
        private var speed : int = 30;
        private var birthTime : int = 4000;
        private var birthInterval : uint;
        private var speedInterval : uint;
        private var bubbles : Dictionary;
        private var tweens : Dictionary;
        private var id : int = 0;
        private var textField : TextField;
        private var score : int = 0;

        public function Game() {
            bubbles = new Dictionary();
            tweens = new Dictionary();
            initBg();
            initBubbles();
            initTouch();
            initScore();
        }

        private function initScore() : void {
            textField = new TextField(320, 50, score.toString(), "Verdana", 14, 0x336699, true);
            textField.hAlign = "right";
            addChild(textField);
        }

        private function initTouch() : void {
            this.addEventListener(TouchEvent.TOUCH, onTouch);
        }

        private function onTouch(event : TouchEvent) : void {
            var touch : Touch = event.getTouch(this, TouchPhase.BEGAN);
            if (touch) {
                var localPos : Point = touch.getLocation(this);
                if (this.hitTest(localPos, true).parent is Bubble) {
                    var bubble : Bubble = this.hitTest(localPos, true).parent as Bubble;
                    if (bubbles[bubble.id] != null) {
                        delete bubbles[bubble.id];
                        bubble.destroy();
                        Starling.juggler.remove(tweens[bubble]);
                        var tween : Tween = new Tween(bubble, 0.5);
                        tween.fadeTo(0);
                        tween.onComplete = onGoOut;
                        tween.onCompleteArgs = [bubble];
                        Starling.juggler.add(tween);
                        increaseScore();
                    }
                }

                trace("Touched object at position: " + localPos);
            }
        }

        private function increaseScore() : void {
            score++;
            textField.text = score.toString();
        }

        private function initBubbles() : void {
            birthInterval = setInterval(bubbleInterval, birthTime);
            speedInterval = setInterval(increaseSpeed, 20000);
        }

        public function pause() {
            clearInterval(birthInterval);
            clearInterval(speedInterval);
        }

        private function increaseSpeed() : void {
            if (birthTime > 1000) {
                birthTime -= 200;
            }
            if (speed > 5) {
                speed -= 2;
            }
        }

        private function bubbleInterval() : void {
            id++;
            var bubble : Bubble = new Bubble();
            bubble.id = id;
            bubble.x = Math.floor(Math.random() * 10) * 32;
            bubble.y = 480 - 32;
            addChild(bubble);
            var tween : Tween = new Tween(bubble, speed, Transitions.EASE_IN);
            tween.moveTo(bubble.x, -32);
            tween.onComplete = onGoOut;
            tween.onCompleteArgs = [bubble];
            Starling.juggler.add(tween);
            bubbles[id] = bubble;
            tweens[bubble] = tween;
        }

        private function onGoOut(bubble : Bubble) : void {
            removeChild(bubble);
            delete tweens[bubble];

            bubble.kill();
            bubble.dispose();
            bubble = null;
        }

        private function initBg() : void {
            bg = new Background();
            addChild(bg);
        }

        public function showLoading() : void {
        }
    }
}
