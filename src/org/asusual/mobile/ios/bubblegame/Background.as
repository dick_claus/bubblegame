package org.asusual.mobile.ios.bubblegame {
    import starling.display.Image;
    import starling.textures.Texture;
    import starling.display.Sprite;

    /**
     * @author Dmitry.Bezverkhiy
     */
    public class Background extends Sprite {
        
        [Embed(source="/../assets/bg.png")]
        private var Bg : Class;
        private var bgImage : Image;
        
        public function Background() {
            var texture : Texture = Texture.fromBitmap(new Bg());
            bgImage = new Image(texture);
            addChild(bgImage);
        }
    }
}
